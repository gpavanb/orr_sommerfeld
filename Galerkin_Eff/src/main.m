% Program to compute the Orr-Sommerfeld matrix for three
% dimensional Poiseuille or Couette flows and to compute
% energy matrix
%

% CLEAN
clc; clearvars; close all;

format long;

% ADD PATHS
addpath('../include/','../include/jacobi/','../include/legendre/');

zi=sqrt(-1);

% inputs
nosmod=input('Enter N the number of OS modes: ');
R=input('Enter the Reynolds number: ');
alp=input('Enter alpha: ');
beta=input('Enter beta: ');

% set up Orr-Sommerfeld matrices A and B
[A,B]=pois_g_eff(nosmod,alp,beta,R);

% compute the optimal
tic
[xs,es]=iord2(sparse(A),sparse(B));
toc
es(1)

% Plot sparsity pattern
subplot(1,2,1);
spy(A);
title('A','FontSize',20);

set(gca,'FontSize',20);
axis square;
subplot(1,2,2);
spy(B);
title('B','FontSize',20);
set(gcf,'Color',[1 1 1]);
set(gca,'FontSize',20);
axis square;