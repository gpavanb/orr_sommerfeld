function [A,B] = pois_g_eff(nosmod,alp,beta,R)

% INITIALIZATION
zi=sqrt(-1);
ak2=alp^2+beta^2;
Nos=nosmod+1;
Nsq=nosmod+1;

% MEAN VELOCITY FUNCTIONS
um = @(x) 1 - x.^2;
um_pp = -2;

% JACOBI POLYNOMIAL USED AS GRID
Ngrid = 100;

%% GENERATE BASIS

% ORDER OF EQUATION
ord = 5;
assert(mod(ord,2) == 1);
m = (ord-1)/2;
alp_g = -m;
bet_g = -m;

% GENERATE GRID
n0 = computeN0(alp_g,bet_g);
Ngrid_eff = Ngrid - n0;
[xx,ww] = legslb(Ngrid_eff);

% COMPUTE BASIS FUNCTIONS AND SCALING FACTORS
phi = zeros(Nos-2*m+1,Ngrid_eff);
sca = zeros(Nos-2*m+1,1);
for i = 1:Nos-2*m + 1
    j = i-1;
    phi(i,:) = gjapoly(j+2*m,-m,-m,xx);
%     phi(i,:) = lepoly(j,xx) + 0*lepoly(j+1,xx) - ((4*j+10)/(2*j+7))*lepoly(j+2,xx) - ...
%                0*((4*j+18)/(2*j+11))*lepoly(j+3,xx) + ((2*j+3)/(2*j+7))*lepoly(j+4,xx) + ...
%                0*((2*j+7)/(2*j+11))*lepoly(j+5,xx); 
end

for i = 1:length(sca)
   j = i-1; 
   sca(i) = 1./sqrt(dot(ww,japolyderm(j+2*m,-m,-m,m,xx).^2));
end

for i = 1:Nos-2*m + 1
   phi(i,:) = sca(i)*phi(i,:); 
end

%% GENERATE MATRICES

% CREATE LHS MATRIX
aMat = zeros(ord,length(xx));
aMat(1,:) = -1/(zi*R);
aMat(2,:) = 0;
aMat(3,:) = alp*um(xx) + 2*ak2/(zi*R);
aMat(4,:) = 0;
aMat(5,:) = -alp*ak2*um(xx) - alp*um_pp - (ak2^2)/(zi*R);

% COMPUTE LHS MATRIX
A = zeros(Nos-2*m + 1,Nos-2*m + 1);
for i = 1:Nos-2*m + 1
    for j = 1:Nos-2*m + 1
       A(i,j) = bil(i,j,m,xx,ww,aMat,phi);
    end
end

% FILTER A
A(abs(A)< 1e-10) = 0;

% CREATE RHS MATRIX
bMat = zeros(ord,length(xx));
bMat(1,:) = 0;
bMat(2,:) = 0;
bMat(3,:) = alp;
bMat(4,:) = 0;
bMat(5,:) = -alp*ak2;

% COMPUTE RHS MATRIX
B = zeros(Nos-2*m + 1,Nos-2*m + 1);
for i = 1:Nos-2*m + 1
    for j = 1:Nos-2*m + 1
       B(i,j) = bil(i,j,m,xx,ww,bMat,phi);
    end
end

% FILTER B
B(abs(B)< 1e-10) = 0;

