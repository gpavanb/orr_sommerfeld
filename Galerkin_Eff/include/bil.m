% Function to compute the corresponding bilinear form for the
% Galerkin matrix

function [y] = bil(i,j,m,yy,ww,b,phi)

y = 0;

bEvenDer = b(1:2:end,:);
bOddDer = b(2:2:end,:);
  for q=1:m+1
    y = y + ((-1)^(m-q+1))*dot(ww', ...
      vecderm(i,m-q+1,yy,ones(1,length(yy)),phi(i,:)).* ...
      conj(vecderm(j,m-q+1,yy,bEvenDer(q,:),phi(j,:))));
  end
  
  for q=1:m
    y = y + ((-1)^(m-q+1))*dot(ww', ...
      vecderm(i,m-q,yy,ones(1,length(yy)),phi(i,:)).* ...
      conj(vecderm(j,m-q+1,yy,bOddDer(q,:),phi(j,:))));
  end
  
end