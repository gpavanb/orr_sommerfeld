% ENSURE x ARE LGL POINTS
% COULD HAVE BEEN GENERATED BUT WHY RECOMPUTE?

function c = vecderm(n,m,x,vec,phiVec)

% COMPUTE DIFFERENTIATION MATRIX
D = legslbdiff(length(x),x);

c = vec'.*phiVec';

for i = 1:m
    c = D*c;
end

end