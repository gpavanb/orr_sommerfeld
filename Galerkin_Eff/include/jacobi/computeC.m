% Function to compute the preceeding coefficient for Jacobi polynomial...
% derivatives

function [c] = computeC(n,alp,bet)

% Compute c
if (alp <= -1 && bet <= -1)
    c = -2*(n + alp + bet + 1);
elseif (alp <= -1 && bet >= -1)
    c = -n; 
elseif (alp >= -1 && bet <= -1)
    c = -n;
elseif (alp > -1 && bet > -1)
    c = 0.5*(n + alp + bet + 1);
end

end