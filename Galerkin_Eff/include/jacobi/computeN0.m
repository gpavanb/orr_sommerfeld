function [n0] = computeN0(alp,bet)

n0 = 0;

if (alp <= -1 && bet <= -1)
    n0 = -(alp + bet); 
elseif (alp <= -1 && bet >= -1)
    n0 = -alp;
elseif (alp > -1 && bet <= -1)
    n0 = -bet;
elseif (alp > -1 && bet > -1)
    n0 = 0;
end

end