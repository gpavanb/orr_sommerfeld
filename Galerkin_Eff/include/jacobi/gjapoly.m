% Generalized Jacobi Polynomials
% See (6.1) in Shen, Wang - Spectral Methods

function [y] = gjapoly(n,alp,bet,x)

n0 = computeN0(alp,bet);

if (alp <= -1 && bet <= -1)
    y = ((1-x).^(-alp)).*((1+x).^(-bet)).*japoly(n-n0,-alp,-bet,x);
elseif (alp <= -1 && bet >= -1)
    y = ((1-x).^(-alp)).*japoly(n-n0,-alp,bet,x);
elseif (alp > -1 && bet <= -1)
    y = ((1+x).^(-bet)).*japoly(n-n0,alp,-bet,x);
elseif (alp > -1 && bet > -1)
    y = japoly(n,alp,bet,x);
end
    
end
