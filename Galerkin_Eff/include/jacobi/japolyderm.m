% y=japolyderm(n,alp,bet,x) computes mth derivative of Jacobi ...
% polynomial of degree n with parameter (alp,bet) at a vector-valued x
% Refer to (3.101,3.102) - Shen, Spectral Methdos for details

function y = japolyderm(n,alp,bet,m,x)

c = (2^m)*gamma(n+alp+bet+m+1)/gamma(n+alp+bet+1);
y = c*gjapoly(n-m,alp+m,bet+m,x);

end