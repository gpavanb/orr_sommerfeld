# Orr_Sommerfeld #

Orr_Sommerfeld is used to solve the Orr-Sommerfeld equation, a fourth-order, variable coefficient equation which appears in stability analysis.

## How do I get set up? ##

Just go to `main.m` in each folder, either `Tau_Coll_SH`, which solves the equation using the Tau-collocation method, as in [1], or in `Galerkin_Eff`, which is how it is solved in [2].

## License ##

Please refer to the LICENSE.pdf in the repository. Note that this code requires PRIOR PERMISSION FROM AUTHORS FOR COMMERCIAL PURPOSES.

## Who do I talk to? ##

* Repo owner or admin : [Pavan Bharadwaj](https://bitbucket.org/gpavanb)
* Other community or team contact : The code was developed at the Flow Physics and Computational Engineering group at Stanford University. Please direct any official queries to [Prof. Matthias Ihme](mailto:mihme@stanford.edu)

## References ##
[1] Schmid, Peter J., and Dan S. Henningson. "Stability and transition in shear flows". Vol. 142. Springer Science & Business Media, 2012.

[2] Govindaraju, Pavan B., Parviz Moin and Matthias Ihme. "Compact and Efficient Basis Functions for Spectral-Galerkin Schemes : Application to Poisson and Orr-Sommerfeld Equations". Journal of Computational Physics (in preparation)  
