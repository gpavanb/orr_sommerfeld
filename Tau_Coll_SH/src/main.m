% Program to compute the Orr-Sommerfeld matrix for three
% dimensional Poiseuille or Couette flows and to compute
% energy matrix
%

% CLEAN
clc; clearvars; close all;

format long;

% ADD PATHS
addpath('../include/');

zi=sqrt(-1);

% inputs
iflow=input('Poiseuille (1) or Couette flow (2) ');
nosmod=input('Enter N the number of OS modes: ');
R=input('Enter the Reynolds number: ');
alp=input('Enter alpha: ');
beta=input('Enter beta: ');

% generate Chebyshev differentiation matrices
[DO,D1,D2,D4]=Dmat(nosmod);

% set up Orr-Sommerfeld matrices A and B
if iflow==1,
[A,B]=pois(nosmod,alp,beta,R,DO,D1,D2,D4);
else
[A,B]=couet(nosmod,alp,beta,R,DO,D1,D2,D4);
end;

% compute the optimal
tic
[xs,es]=iord2(A,B);
toc
es(1)
