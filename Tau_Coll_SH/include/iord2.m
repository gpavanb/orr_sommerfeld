function [xs,es]=iord2(A,B)

[v,e]=eigs(A,B,1,'sm');
e=diag(e);
xs = v;
es = e;
end